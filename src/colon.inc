%define next_pointer 0

%macro colon 2
	%ifid %2
		%2:					; curr pointer
		dq next_pointer		; place next pointer
		%define next_pointer %2	; redefine next
	%else
		%error "Id should be a pointer"
	%endif

	%ifstr %1
		db %1, 0			; value
	%else
		%error "Content should be a string"
	%endif
%endmacro
