ASM = nasm
ASMF = -f elf64
LD = ld
build=./build
src=./src
SOURCES = $(shell find ./src -name '*.asm')
OBJS = $(patsubst $(src)/%.asm, $(build)/%.o, $(SOURCES))

.PHONY: clean
all: build program 

build/%.o: src/%.asm
	$(ASM) $(ASMF) -o $@ $<

program: $(OBJS)
	$(LD) -o $@ $^

clean:
	rm -rf build/*

