%include "src/lib.inc"
%include "src/colon.inc"
%include "src/dict.inc"
%include "src/words.inc"

%define ptr_len 8
%define buffer_sz 256

global _start

section .rodata
welcome_ms: db "Hello, whatcha lookin' for?", 0
prompt_ms: db "Query -> ", 0
inp_ovf_ms: db "This query wont ever fit in me!", 0
not_found_ms: db "Zdes' syra net!!!", 0
found_ms: db "Check this out -> ", 0

exit_cmd: db ":exit", 0

section .bss
buffer: resb buffer_sz

global _start

section .text
_start:
    mov rdi, welcome_ms
    call    print_string
    call    print_newline

    .read_loop:
    mov rdi, prompt_ms
    call    print_string
    mov rdi, buffer
    mov rsi, buffer_sz
    call    read_word
    test    rax, rax
    jz .read_err
    push rdx
    push rax
    mov rdi, rax
    mov rsi, exit_cmd
    call string_equals
    cmp rax, 1
    je exit
    pop rax
    jmp .find

    .read_err:
    mov rdi, inp_ovf_ms
    call    print_error
    jmp .read_loop

    .find:
    mov rdi, rax
    mov rsi, list_start
    call    find_word
    test    rax, rax
    jz  .find_fail
    jmp .find_succ

    .find_fail:
    mov rdi, not_found_ms
    call    print_error
    call    print_newline
    add rsp, ptr_len
    jmp .read_loop

    .find_succ:
    push    rax
    mov rdi, found_ms
    call    print_string
    pop rdi
    add rdi, ptr_len
    add rdi, [rsp]
    add rsp, ptr_len
    inc rdi
    call    print_string
    call    print_newline
    jmp .read_loop
