%include "src/lib.inc"

%define ptr_len 8

global find_word

;rdi - string pointer
;rsi - list pointer
section .text
find_word:
    .loop:
    test    rsi, rsi    ; check this el not null
    jz  .break
    add rsi, ptr_len    ; skip next_ptr
    push rdi
    push rsi
    call    string_equals   ; compare
    pop rsi
    pop rdi
    sub rsi, ptr_len
    test    rax, rax    ; found?
    jnz .found
    mov rsi, [rsi]
    jmp .loop
    .break:
    xor rax, rax
    ret
    .found:
    mov rax, rsi
    ret
